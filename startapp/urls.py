from django.urls import path

from . import views

app_name= "startapp"

urlpatterns = [
    path('', views.index, name='index'),
    path('lang/<str:lang>/', views.setLang, name='setLang'),
    path('lang/<str:lang>/<path:path>/', views.setLang, name='setLang'),
    #path('lang/<str:lang>/<str:info>/<str:infoType>/', views.setLang, name='setLang'),
    #path('lang/<str:lang>/<str:info>/<str:infoType>/<str:roomType>/<str:roomName>/', views.setLang, name='setLang'),
    path('generatePassword/', views.generatePassword, name="generatePassword"),
    path('create/', views.create, name="create"),
    path('info/<str:infotype>/', views.info, name="info"),
    path('info/<str:infotype>/<str:roomType>/<str:roomName>/', views.info, name="info"),
]

