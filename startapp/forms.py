from django import forms
from django.utils.translation import gettext

class createRoomForm(forms.Form):
    roomName = forms.CharField(max_length=100)
    roomType = forms.CharField(max_length=30)

    customGuestRights = forms.BooleanField(required = False)
    guestRightAddNote = forms.BooleanField(required = False)
    guestRightLikeNote = forms.BooleanField(required = False)
    guestRightAddImg = forms.BooleanField(required = False)
    guestRightAddReply = forms.BooleanField(required = False)
    guestRightEditNote = forms.BooleanField(required = False)
    guestRightEditReply = forms.BooleanField(required = False)
    guestRightRemoveNote = forms.BooleanField(required = False)
    guestRightRemoveReply = forms.BooleanField(required = False)
    guestRightChangeGuestRights = forms.BooleanField(required = False)
    guestRightAddPassword = forms.BooleanField(required = False)
    guestRightChangePassword = forms.BooleanField(required = False)
    guestRightRemovePassword = forms.BooleanField(required = False)
    guestRightChangeModeratorPassword = forms.BooleanField(required = False)
    guestRightDeleteRoom = forms.BooleanField(required = False)
    changePassword = forms.BooleanField(required = False)
    
    modPassword = forms.CharField(required=False,max_length=100, widget=forms.PasswordInput)

    passwordProtection = forms.BooleanField(required=False)
    password = forms.CharField(required=False, max_length=100, widget=forms.PasswordInput)
