from django.shortcuts import render
from django.utils import translation
from django.utils import timezone
from django.utils.translation import gettext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.hashers import check_password, make_password
import datetime
from os import urandom
# Create your views here.

#from wall.models import Room as WallRoom
from wall.models import Room

from .forms import *

def index(request):
    return render(request, 'startapp/index.html')
def create(request):
    form = createRoomForm(request.POST)

    if form.is_valid():
        roomName = form.cleaned_data["roomName"]
        roomType = form.cleaned_data["roomType"]
        if not request.session.session_key:
            request.session.save()

        if (Room.objects.filter(roomName=form.cleaned_data["roomName"]).count() == 0 
                and (form.cleaned_data["passwordProtection"] or form.cleaned_data["customGuestRights"])):
            #pw = make_password(request.POST["password"], salt=urandom(25).hex())
            #modpw = make_password(request.POST["modPassword"], salt=urandom(25).hex())
            room = Room(roomName = roomName,
                        pubDate = timezone.now(),
                        lastModified = timezone.now(),
                        passwordProtection = form.cleaned_data["passwordProtection"],
                        password=make_password(form.cleaned_data["password"], salt=urandom(25).hex()) if form.cleaned_data["passwordProtection"] else "",
                        customGuestRights = form.cleaned_data["customGuestRights"],
                        modPassword = make_password(form.cleaned_data["modPassword"], salt=urandom(25).hex()) if form.cleaned_data["customGuestRights"] else "",
                    )
            room.save()
            if form.cleaned_data["passwordProtection"]:
                room.authorizedusers_set.create(sessionKey=request.session.session_key,
                        expireDate=timezone.now() + datetime.timedelta(days=14),
                        lastActive=timezone.now())
                room.save()
            if form.cleaned_data["customGuestRights"]:
                room.authorizedmoderators_set.create(sessionKey=request.session.session_key,
                        expireDate=timezone.now() + datetime.timedelta(days=14),
                        lastActive=timezone.now())
                room.customguestrights_set.create(
                        addNote = form.cleaned_data["guestRightAddNote"],
                        likeNote = form.cleaned_data["guestRightLikeNote"],
                        addImg = form.cleaned_data["guestRightAddImg"],
                        addReply = form.cleaned_data["guestRightAddReply"],
                        editNote = form.cleaned_data["guestRightEditNote"],
                        editReply = form.cleaned_data["guestRightEditReply"],
                        removeNote = form.cleaned_data["guestRightRemoveNote"],
                        removeReply = form.cleaned_data["guestRightRemoveReply"],
                        changeGuestRights = form.cleaned_data["guestRightChangeGuestRights"],
                        addPassword = form.cleaned_data["guestRightAddPassword"],
                        removePassword = form.cleaned_data["guestRightRemovePassword"],
                        changePassword = form.cleaned_data["guestRightChangePassword"],
                        changeModeratorPassword = form.cleaned_data["guestRightChangeModeratorPassword"],
                        deleteRoom = form.cleaned_data["guestRightDeleteRoom"],       
                        )
                room.save()
        if Room.objects.filter(roomName=form.cleaned_data["roomName"]).count() == 1:
           #     and ("passwordProtection" in request.POST.keys() or "customGuestRights" in request.POST.keys())):
            room = Room.objects.filter(roomName=form.cleaned_data["roomName"])[0]
            if form.cleaned_data["passwordProtection"] and room.passwordProtection and check_password(form.cleaned_data["password"], room.password):
                if room.authorizedusers_set.filter(sessionKey=request.session.session_key).count() == 0:
                    room.authorizedusers_set.create(sessionKey=request.session.session_key,
                            expireDate=timezone.now() + datetime.timedelta(days=14),
                            lastActive=timezone.now())
                elif room.authorizedusers_set.filter(sessionKey=request.session.session_key).count() == 1:
                    user = room.authorizedusers_set.filter(sessionKey=request.session.session_key)[0]
                    user.lastActive=timezone.now()
                    user.save()
                else: 
                    return 0
            if form.cleaned_data["customGuestRights"] and room.customGuestRights and check_password(form.cleaned_data["modPassword"], room.modPassword):
                if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 0:
                        room.authorizedmoderators_set.create(sessionKey=request.session.session_key,
                                expireDate=timezone.now() + datetime.timedelta(days=14),
                                lastActive=timezone.now())
                elif room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1:
                    user = room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0]
                    user.lastActive = timezone.now()
                    user.save()
                else: return 0

            if ((form.cleaned_data["passwordProtection"] and not check_password(form.cleaned_data["password"], room.password))
                    or (form.cleaned_data["customGuestRights"] and not check_password(form.cleaned_data["modPassword"], room.modPassword))):
                        
                context = {"roomName": form.cleaned_data["roomName"],
                           "roomType": form.cleaned_data["roomType"],
                           "strTitle": gettext("Room %(roomName)s already exists") % {"roomName": form.cleaned_data["roomName"]},
                           "buttonText":  gettext("Join room"),
                           }

                context["msg"] = gettext("This room already exists with the following conditions:")
                context["msglist"] = []

                if room.passwordProtection:
                    context["msglist"] += [gettext("Password protection")]
                else:
                    context["msglist"] += [gettext("NO (!) password protection")]
                if room.customGuestRights:
                    context["msglist"] += [gettext("customized Guest Rights")]
                else:
                    context["msglist"] += [gettext("NO (!) Guest Rights")]

                context["aftermsglist"] = gettext("If you created the room, and know the relevant passwords, go ahead. Otherwise go back and create a new room, with a different room name.")


                return render(request, 'wall/roomAlreadyExists.html', context)

        # for a normal room, just go ahead
        return HttpResponseRedirect('/' + roomType + '/' + roomName)
    else:
        return HttpResponseRedirect('/')

def info(request, infotype, roomType=None, roomName=None):
    context = {}
    if type(roomType) != type(None):
        context["roomType"] = roomType
    if type(roomName) != type(None):
        context["roomName"] = roomName
    context["info"] = True
    context["infoType"] = infotype
    return render(request, "general/" + infotype + ".html", context)
    
#def setLang(request, lang, info=None, infoType=None, roomType=None, roomName=None):
def setLang(request, lang, path=None):
    response = HttpResponseRedirect('/')
    #translation.activate(lang)
    #response.set_cookie("django_language", lang)
    request.session["django_language"] = lang
    """URL = "/"
    if type(info) != type(None):
        URL += "info/"
    if type(infoType) != type(None):
        URL += infoType + "/"
    if type(roomType) != type(None):
        URL += roomType + "/"
    if type(roomName) != type(None):
        URL += roomName + "/" """

    return HttpResponseRedirect("/" + path if type(path) != type(None) else "/")

def generatePassword(request):
    return HttpResponse(urandom(15).hex())
