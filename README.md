# Stickies

Stickies is a simple open-source and privacy respecting online bulletin board based on [Django](https://djangoproject.com), where you, your friends or your colleagues can share ideas on sticky notes pinned to a wall. The notes get synced (with maybe a few seconds delay) and you can get creative!

For now you can only put notes on a wall (with images), like them and answer to them, but more features are planned! (e.g. adding a different base layout where you can arrange columns with titles of your own!)

## Table of contents

[[_TOC_]]

## Features
 - General
   - fast
   - simplicity by design
   - Privacy respecting!
   - only strictly necessary cookies are stored (actually usable without cookies, though password protected rooms don't work)
   - Translations (currently only english and german are supported, [**help translating**](#translation)) 
 - Bulletin board
   - Put notes on a board (like sticky notes)
   - Add images to your notes
   - reply to notes
   - like notes 
   - Choose how to order the notes! Likes? Replies? Age? A mix of all is provided, where new ones show up first, and older ones are sorted by a combination of likes and comments
   - edit replies and notes
   - Something wrong with a note or reply? Just delete it!
   - Protect rooms with a password, so only people with password can view the bulletin board!
   - Customize Rights for guests! Don't want everyone to be able to delete notes or (accidentally) delete the room? Protect features with a moderator password!
   - Works just fine on mobile phones!
 - **(Upcoming)** New room type, where you have more control over the columns, add and remove them, give them titles, add notes to them

## Screenshots
<img src="/res/screenshots/01.png" alt="Creating a room view" width="33%"> <img src="/res/screenshots/02.png" alt="A wall room with notes in action" width="33%"> <img src="/res/screenshots/03.png" alt="Add a note view" width="33%"> 
## Installation

### Install python, django and pip and other dependencies

    sudo apt-get install python3 python3-pip python3-pil
	pip install django

### Clone repo

clone the repo and go into directory

    git clone "https://gitlab.com/edvsturahd/stickies.git
    cd stickies/

### Preparational steps
#### Django secret key
Create a django secret key from within the stickies folder where the `manage.py` file is located.

    python3 manage.py shell -c 'from django.core.management import utils; print("SECRET_KEY = \"%s\"" % utils.get_random_secret_key())' > stickies/djangosecret.py

#### Adjust settings
In the `stickies/settings.py` file change the values:

    [...]
    DEBUG = False # Should be false for production
    [...]
    ALLOWED_HOST = ["example.com", "server.ip"]
    [...]
    LANGUAGE_CODE = 'your preferred language' #not necessary
    [...]
    TIME_ZONE = 'your time zone"

it seems that setting the actual IP of the server in the ALLOWED_HOST list helps. That is meant with `server.ip`.

#### Database setup
to initialize the database run

    python3 manage.py makemigrations
	python3 manage.py migrate

#### Collect static files
Collect your static files in `static/` folder within the main `stickies` directory
You can adjust this path in the `stickies/settings.py` by adjust the STATIC_ROOT parameter

    python3 manage.py collectstatic

#### The lazy developer
In `wall/static/wall/wall.js` you'll find in one of the last lines

    /* Uncomment the following line for production */
    /* this line makes sure, notes get synced */
    setInterval(checkUpdate,5000);

make sure the `setInterval(...)` function is uncommented! This function is syncing between different clients making it really important for production.
This line **should** always be uncommented in the git, however for testing purposes it is often commented, and the devs might not always remember to make sure at every commit, that it is uncommented

#### Install gunicorn
So far Stickies is tested with gunicorn, and this How-To will stick to gunicorn. Feel free to try and deploy with other methods, see the [Django tutorial on deploying](https://docs.djangoproject.com/en/3.2/howto/deployment/).

Install gunicorn

    sudo apt-get install gunicorn # or pip install gunicorn


### Setting up the server
#### Testing with standalone gunicorn (skippable)
**CAREFUL** it is strongly recommended using Guincorn behind a proxy server. See the [following chapter on how to setup gunicorn behind nginx](#setting-up-nginx-as-proxy-server).

Remember you should setup a proxy server. If you want to test server (**the risk is up to you**), run gunicorn within the stickies directory, where the `manage.py` file is

    gunicorn --bind stickies.example.com:80 stickies.wsgi

##### Setting up SSL
**CAREFUL** it is still recommended using Gunicorn behind a proxy server. See the [following chapter on nginx as proxy](#setting-up-nginx-as-proxy-server).

Use your favourite method of getting a SSL certificate (e.g. [Let's encrypt](https://letsencrypt.org/) with [Certbot](https://certbot.eff.org/)).

With your SSL certificates in place run

    gunicorn --certfile=/etc/letsencrypt/live/stickies.example.com/fullchain.pem --keyfile=/etc/letsencrypt/live/stickies.example.com/privkey.pem --bind stickies.example.com:443 stickies.wsgi

where you might need to adjust the paths.

#### Setting up nginx as proxy server 


It's probably better to hide gunicorn behind a proxy (e.g. nginx). The following part ist based on [Deploying gunicorn](https://docs.gunicorn.org/en/latest/deploy.html).

Install nginx

    sudo apt-get install nginx

Copy the adjusted config which is based on the the [Gunicorn docs](https://docs.gunicorn.org/en/latest/deploy.html) [File](https://github.com/benoitc/gunicorn/blob/master/examples/nginx.conf), slightly modified by appending `proxy_buffering off;` and add some SSL options you need to uncomment and adjust.

	cp res/conf/nginx-gunicorn.conf /etc/nginx/nginx.conf

Open `/etc/nginx/nginx.conf` and adjust the parameters
    
    [...]
    server_name = example.com; # set correct host(s) for your site
    [...]
    root = /path/to/stickies; # earlier we collected all static files in one dir
    [...]
    alias /path/to/stickies/static/; # replace path to your stickies folder
    [...]
    alias /path/to/stickies/media/; # same here!
    
	
Then restart nginx

    systemctl restart nginx

Now run gunicorn on localhost

    gunicorn -u nobody -g nogroup --bind 127.0.0.1:8000 stickies.wsgi

Now your server should be up and running. Enjoy!

##### Setting up SSL (optional)
Use either the previously generated certificates or if you haven't done so get your SSL certificates (e.g. [Let's encrypt](https://letsencrypt.org/) with [Certbot](https://certbot.eff.org/)).

Uncomment and adjust the SSL parameters in the `/etc/nginx/nginx.conf` file

    [...]
    # SSL
    
    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/stickies.example.com/fullchain.pem; # replace path to your cert
    ssl_certificate_key /etc/letsencrypt/live/stickies.example.com/privkey.pem; # replace path to your cert
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    [...]

This is all, now you sohuld have a running server behind SSL. Enjoy even more :)

##### Troubleshhoting
###### Static and media files not loaded
If the page loads, but doesn't look like it's supposed to look like, it is most likely a problem with nginx not serving static and mediafiles due to permissions. Try to open `stickies.example.com/media/test.txt` where the test file doesn't need to exist, we just want to know if you get a 403 Forbidden error, or a 404 not found error.
If you get a 403 Forbidden error, this means nginx has some problem with permissions.
nginx needs permission in the parent Folder of stickies as well! Try:

    sudo chown nobody:nogroup /path/to/stickies/parent/folder -R
	# e.g. if you installed stickies to /var/www/html/stickies
    # sudo chown nobody:nogroup /var/www/html -R

and make sure permissions are properly set

    sudo chmod 755 /path/to/stickies/parent/folder -R

Now try to access `stickies.example.com/media/test.txt` and see if it gets you a 404 error. If you get a 404 everything should work fine now.

**If it still doesnt work**, try to replace the nobody:nogroup to www-data:www-data in

in the nginx.conf file: `/etc/nginx/nginx.conf` `user www-data www-data;`

In the stickies parent folder `sudo chown www-data:www-data /path/to/stickies/parent/folder -R`

When running gunicorn: `gunicorn -u www-data -g www-data --bind 127.0.0.1:8000 stickies.wsgi`

# Updating Stickies

Until there are proper releases, use git.
Use the git stash function to store your local changes (e.g. in the `stickies/settings.py` file)
From within the `stickies` folder where the manage.py file is located run

    git stash # store local changes
    git pull # update
    git stash apply # restore local changes

Update the database tables

    python3 manage.py makemigrations
    python3 manage.py migrate

Use the `updatedb.py` script to update database values.

    python3 manage.py shell < updatedb.py


## Customizations

For now customizations are not easily possible. Feel fry to go through all CSS files and change the values to your taste - but it's gonna be hard work.
Making this more easy is a feature planned for the Future!

## Translation

You want to help translate? It is really easy!

### Help translating existing Languages:
in the stickies folder, where the manage.py file is located run 

    django-admin makemessages -l langue-code # eg django-admin makemessages -l de

now locate the `*.po` files in `startapp/locale/language-code/...` or `wall/locale/language-code/...` and translate the messages in the `.po` files. When you are done or want to test something run

	django-admin compilemessages

now you should see the translated messages

### Add a new language

Open the `stickies/settings.py` file with your favourite editor and add a new language to the `LANGUAGES` list using a language code, and a language name. After that run 
    
    django-admin makemessages -l langue-code # eg django-admin makemessages -l en_GB

now locate the `*.po` files in `startapp/locale/language-code/...` or `wall/locale/language-code/...` and translate the messages in the `.po` files. When you are done, or want to test something run

	django-admin compilemessages

now you should see the translated messages
