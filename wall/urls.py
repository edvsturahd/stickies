from django.urls import path

from . import views

app_name= "wall"

urlpatterns = [
        path('<str:roomName>/', views.room, name='index'),
        path('<str:roomName>/error/<str:errorType>/', views.error, name='error'),
        path('<str:roomName>/addChangePassword/', views.addChangePassword.as_view(), name='addChangePassword'),
        path('<str:roomName>/removePassword/', views.removePassword.as_view(), name='removePassword'),
        path('<str:roomName>/checkUpdate/', views.checkUpdate, name='checkUpdate'),
        path('<str:roomName>/update/', views.update, name='update'),
        path('<str:roomName>/delete/', views.delete.as_view(), name='delete'),
        path('<str:roomName>/moderatorLogin/', views.moderatorLogin.as_view(), name='moderatorLogin'),
        path('<str:roomName>/moderatorLogout/', views.moderatorLogout, name='moderatorLogout'), 
        path('<str:roomName>/roomLogout/', views.roomLogout, name='roomLogout'), 
        path('<str:roomName>/editGuestRights/', views.editGuestRights.as_view(), name='editGuestRights'),
        path('<str:roomName>/<int:noteId>/like/', views.addLike, name='like'),
        path('<str:roomName>/<int:noteId>/reply/', views.reply.as_view(), name='reply'),
        path('<str:roomName>/<int:noteId>/remove/', views.removeNote, name='removeNote'),
        path('<str:roomName>/<int:noteId>/editNote/', views.editNote.as_view(), name='editNote'),
        path('<str:roomName>/<int:noteId>/images/', views.images, name='editNote'),
        path('<str:roomName>/<int:noteId>/<int:replyId>/editReply/', views.editReply.as_view(), name='editReply'),
        path('<str:roomName>/<int:noteId>/<int:replyId>/remove/', views.removeReply, name='removeReply'),
        path('<str:roomName>/session/<str:key>/<str:val>/', views.sessionChange, name='session'),
        path('<str:roomName>/addnote/', views.addNote.as_view(), name='addnote'),
]

