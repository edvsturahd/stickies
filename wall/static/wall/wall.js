//var notes =  ;
var newGuestRights;
if (ncols != "auto") {ncols = Number(ncols);}

function findLeastCharCol() {
  let data = []
  for (let i = 1; i < ncols + 1; i++) {
    data.push(document.getElementById("col" + i).scrollHeight)
  }
  return (data.indexOf(Math.min(...data)) + 1)
}
function checkUpdate() {
  const http = new XMLHttpRequest();
  http.open("GET", "checkUpdate/");
  http.send();
  http.onreadystatechange = function () {
	if (this.status == 404) {
	  //console.log("Error 404 recieved");
      window.location.replace("/wall/" + roomName + "/error/roomRemoved/")
	}
    if (this.readyState == 4 && this.status==200) {
      data = JSON.parse(http.responseText);
	  if (("msg" in data && data["msg"] == "passwordRequired") || passwordProtection != data["passwordProtection"]) {
	    window.location.reload(true);
	  }
      console.log(Number(String(data["lastModified"]).replace(",", ".")), Number(String(lastModified).replace(",",".")), Number(String(data["lastModified"]).replace(",",".")) > Number(String(lastModified).replace(",",".")))
      if (Number(String(data["lastModified"]).replace(",",".")) > Number(String(lastModified).replace(",","."))) {
        requestUpdate();
      }
    }
  }
}
function requestUpdate() {
  const http = new XMLHttpRequest();
  http.open("GET", "update/");
  http.send();
  http.onreadystatechange = function () {
    if (this.readyState == 4 && this.status==200) {
      data = JSON.parse(http.responseText);
	  newGuestRights = data["guestRightsJS"]

	  let guestRightsChange = false;
	  for (key in newGuestRights) {
		if (newGuestRights[key] != guestRights[key]) { guestRightsChange = true;}
	  }
	  if (guestRightsChange || customGuestRights != data["customGuestRights"]) {window.location.reload(true)}
	  //if (guestRightsChange) {window.location.reload(true)}

	  lastModified = data["lastModified"]
      notes = data["notes"];
      arrangeCols();
    }
  }
}
function arrangeCols() {
  console.log(notes)
  yOffset = document.documentElement.scrollTop;
  let i = 1;
  while (document.getElementById("col" + i) != null && i <= 10) {
    let col = document.getElementById("col" + i);
    if (col != null) { col.innerHTML = ""; col.remove()};
    i += 1;
  }

  let ncolsautobefore = false;

  if (ncols == "auto") {
    ncolsautobefore = true;
    let w = document.getElementById("main").scrollWidth;
    ncols = Math.min(Math.round(w/(275+1/50*w)),10)
  }

  for (let i = 1; i <= ncols; i++) {
    let col = document.createElement("div");
    col.setAttribute("id","col" + i);
    col.classList.add("col");
    col.setAttribute("style", "width: " + ((100-2*ncols)/ncols).toString() + "%;");

    let divMain = document.getElementById("main");
    divMain.append(col);
  }

  for (let i = 0; i < notes.length; i++) {

    let col = document.getElementById("col" + findLeastCharCol());
    let note = document.createElement("div");
    note.classList.add("note");

	if (notes[i]["imgs"].length > 0) {
	  let divImg = document.createElement("div");

      let aImgLink = document.createElement("a");
	  aImgLink.href = "/wall/" + roomName + "/" + notes[i]["id"] + "/images/";
	  //aImgLink.target = "_blank"
	  
	  divImg.classList.add("divImg");
	
	  for (let j = 0; j < Math.min(notes[i]["imgs"].length, 3); j++) {
		console.log(j)
	    let img = document.createElement("img")
		img.src = "/media/" + notes[i]["imgs"][j]["file"].substring(0,notes[i]["imgs"][j]["file"].lastIndexOf(".")) + "-cropped" + notes[i]["imgs"][j]["file"].substring(notes[i]["imgs"][j]["file"].lastIndexOf("."), notes[i]["imgs"][j]["file"].length);
		if (j == 0) {
		  img.setAttribute("style","width: 90%;")
		}
		else {
		  img.setAttribute("style", "width: 44%; margin-left: 1%; margin-right:1%;")
		}
		divImg.append(img)
	  }
	  if (notes[i]["imgs"].length > 3) {
	    pMoreImages = document.createElement("p");
	    pMoreImages.setAttribute("style", "font-size: 10px; display: inline-block; color:black");
		pMoreImages.innerHTML = "Click to see " + (notes[i]["imgs"].length - 3) + " more images";
		divImg.append(pMoreImages);
	  }
	  
	  aImgLink.append(divImg);
	  note.append(aImgLink);
	}
	

    pnoteText = document.createElement("p");
    pnoteText.innerHTML = notes[i].noteText;
    note.append(pnoteText);
    
    let divDateUser = document.createElement("div");
    divDateUser.classList.add("noteUserDate");

    let pdate = document.createElement("p")
    pdate.classList.add("pDate");

    let datestring = ""
    let now = new Date()
    let noteDate = new Date(parseInt(notes[i]["pubDate"])*1000)
    
    datestring =noteDate.getHours().toString().padStart(2,"0") + ":" +  noteDate.getMinutes().toString().padStart(2,"0");
    datestring = noteDate.getDate().toString().padStart(2,"0") + "/" + (noteDate.getMonth() + 1).toString().padStart(2,"0") + " " + datestring


    pdate.innerHTML =datestring;

    let pname = document.createElement("p")
    pname.setAttribute("style","float: left;");
    //pname.classList.add("pName");
    pname.innerHTML = notes[i]["name"];

    divDateUser.append(pname);
    divDateUser.append(pdate);

    note.append(divDateUser);

    /*divReplies = document.createElement("div");
    divReplies.classList.add("replies")
    /* To Do!!! */
    //note.append(divReplies);



    divInteract = document.createElement("div");
    divInteract.classList.add("noteInteract");
    
    pLikes = document.createElement("p");
    pLikes.classList.add("pLikes");
    pLikes.innerHTML = notes[i]["likes"] + " likes";
    divInteract.append(pLikes);

    pButtons = document.createElement("p");
    pButtons.classList.add("pButtons");

	if (!customGuestRights || guestRights["likeNote"] || isMod) {  
      buttonLike = document.createElement("a");
      buttonLike.classList.add("buttonLike");
      buttonLike.href = "/wall/" + roomName + "/" + notes[i]["id"] + "/like/";
    
      buttonLikeImg = document.createElement("img");
      buttonLikeImg.classList.add("buttonLikeImg");
      buttonLikeImg.src = imgLikeSrc;
    
      buttonLike.append(buttonLikeImg);
      buttonLike.innerHTML += strLike;

      pButtons.append(buttonLike);
	}
    
	if (!customGuestRights || guestRights["addReply"] || isMod) {  
	  buttonReply = document.createElement("a");
      buttonReply.classList.add("buttonLike")
      buttonReply.href = "/wall/" + roomName + "/" + notes[i]["id"] + "/reply/";
    
      buttonReplyImg = document.createElement("img");
      buttonReplyImg.classList.add("buttonLikeImg");
      buttonReplyImg.src = imgReplySrc;
    
      buttonReply.append(buttonReplyImg);
      buttonReply.innerHTML += strReply;
     
      pButtons.append(buttonReply);
	}

	if (!customGuestRights || guestRights["editNote"] || isMod) {  
  	  buttonEdit = document.createElement("a");
	  buttonEdit.classList.add("buttonLike");
	  buttonEdit.href = notes[i]["id"] + "/editNote";
	
	  buttonEditImg = document.createElement("img");
	  buttonEditImg.classList.add("buttonLikeImg");
	  buttonEditImg.src = imgEditSrc;
	
	  buttonEdit.append(buttonEditImg);
	  buttonEdit.innerHTML += strEdit;
	
	  pButtons.append(buttonEdit);
	}
    
	if (!customGuestRights || guestRights["removeNote"] || isMod) {  
      buttonRemove = document.createElement("a");
      buttonRemove.classList.add("buttonLike")
      buttonRemove.href = "/wall/" + roomName + "/" + notes[i]["id"] + "/remove/";
      buttonRemove.onclick = function () { return confirm(strConfirmNoteRemove);}
    
      buttonRemoveImg = document.createElement("img");
      buttonRemoveImg.classList.add("buttonLikeImg");
      buttonRemoveImg.src = imgRemoveSrc;
    
      buttonRemove.append(buttonRemoveImg);
      buttonRemove.innerHTML += strRemove;
    
      pButtons.append(buttonRemove);
	}

    divInteract.append(pButtons)

    note.append(divInteract);

    if (notes[i]["replies"].length > 0) {
      let divReplies = document.createElement("div");
      divReplies.classList.add("replies")
      
      let pDivRepliesHeading = document.createElement("p");
      pDivRepliesHeading.classList.add("pDivRepliesHeading")
      pDivRepliesHeading.innerHTML = "<hr><b>" + strReplies +"</b>"

      divReplies.append(pDivRepliesHeading)

      for (let j = 0; j < notes[i]["replies"].length; j++) {
        let divReply = document.createElement("div");
        divReply.classList.add("reply")
        if (j != 0) {
          divReply.innerHTML = "<hr>"
        }
        if (notes[i]["replies"][j]["name"] != "anonymous") {
         divReply.innerHTML += notes[i]["replies"][j]["name"] + " ";
        }
        let replyDate = new Date(parseInt(notes[i]["replies"][j]["pubDate"])*1000)
        datestring =replyDate.getHours().toString().padStart(2,"0") + ":" +  replyDate.getMinutes().toString().padStart(2,"0");
        datestring = replyDate.getDate().toString().padStart(2,"0") + "/" + (replyDate.getMonth() + 1).toString().padStart(2,"0") + " " + datestring
        divReply.innerHTML += datestring + ": "

        divReply.innerHTML += notes[i]["replies"][j]["replyText"];
        divReplies.append(divReply)
        
        /*pButtons = document.createElement("p");
        pButtons.classList.add("pButtonsReply");*/
	
        
		if (!customGuestRights || guestRights["removeReply"] || isMod) {  
          buttonRemove = document.createElement("a");
          buttonRemove.classList.add("buttonRemoveComment")
          buttonRemove.href = "/wall/" + roomName + "/" + notes[i]["id"] + "/" + notes[i]["replies"][j]["id"] + "/remove/";

          buttonRemove.onclick = function () { return confirm(strConfirmReplyRemove);}
    
          buttonRemoveImg = document.createElement("img");
          buttonRemoveImg.classList.add("buttonRemoveCommentImg");
          buttonRemoveImg.src = imgRemoveSrc;
    
          buttonRemove.append(buttonRemoveImg);
          buttonRemove.innerHTML += strRemove;
          //pButtons.append(buttonRemove);
          //divReply.append(pButtons);
          divReply.append(buttonRemove);
		}
		
		if (!customGuestRights || guestRights["editReply"] || isMod) {  
		  buttonEdit = document.createElement("a");
    	  buttonEdit.classList.add("buttonRemoveComment");
     	  buttonEdit.href = notes[i]["id"] + "/" + notes[i]["replies"][j]["id"] + "/editReply";
	
          buttonEditImg = document.createElement("img");
          buttonEditImg.classList.add("buttonRemoveCommentImg");
     	  buttonEditImg.src = imgEditSrc;
	
	      buttonEdit.append(buttonEditImg);
	      buttonEdit.innerHTML += strEdit;
	
	      divReply.append(buttonEdit);
		}

      }


      note.append(divReplies);
    }


    col.append(note);
  }
  if (ncolsautobefore) {
    ncols = "auto";
    ncolsautobefore = false;
  }
  document.documentElement.scrollTop = yOffset;
}

arrangeCols();

window.onresize = function(e) {
  if (ncols == "auto") {
    arrangeCols();
  }
};

/* Uncomment the following line for production */
/* this line makes sure, notes get synced */
setInterval(checkUpdate,5000);
