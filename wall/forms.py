from django import forms
from django.utils.translation import gettext

class addNoteForm(forms.Form):
    name = forms.CharField(label=gettext("name"), max_length=50)
    storeName = forms.BooleanField(required=False, label=" Store Name")
    images = forms.ImageField(required=False, widget=forms.ClearableFileInput(attrs={'multiple': True}))
    noteText = forms.CharField(required=False, max_length=2000)

class addReplyForm(forms.Form):
    name = forms.CharField(label=gettext("name"), max_length=50)
    storeName = forms.BooleanField(required=False, label=" Store Name")
    replyText = forms.CharField(required=False,max_length=2000)

class editNoteForm(addNoteForm):
    removeImages = forms.BooleanField(required=False, label=" Remove Images")

class deleteRoomForm(forms.Form):
    password = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput) 

class addChangePasswordForm(forms.Form):
    oldpassword = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
    newpassword = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
    newpasswordrep = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)

class removePasswordForm(deleteRoomForm):
    pass

class moderatorLoginForm(deleteRoomForm):
    pass
class roomPasswordForm(deleteRoomForm):
    pass

class editGuestRightsForm(forms.Form):
    customGuestRights = forms.BooleanField(required = False)
    guestRightAddNote = forms.BooleanField(required = False)
    guestRightLikeNote = forms.BooleanField(required = False)
    guestRightAddImg = forms.BooleanField(required = False)
    guestRightAddReply = forms.BooleanField(required = False)
    guestRightEditNote = forms.BooleanField(required = False)
    guestRightEditReply = forms.BooleanField(required = False)
    guestRightRemoveNote = forms.BooleanField(required = False)
    guestRightRemoveReply = forms.BooleanField(required = False)
    guestRightChangeGuestRights = forms.BooleanField(required = False)
    guestRightAddPassword = forms.BooleanField(required = False)
    guestRightChangePassword = forms.BooleanField(required = False)
    guestRightRemovePassword = forms.BooleanField(required = False)
    guestRightChangeModeratorPassword = forms.BooleanField(required = False)
    guestRightDeleteRoom = forms.BooleanField(required = False)
    changePassword = forms.BooleanField(required = False)
    
    modPassword = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
    oldModPassword = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
    newModPassword = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
    newModPasswordRep = forms.CharField(required=False,max_length=32, widget=forms.PasswordInput)
