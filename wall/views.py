from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy, gettext
from django.utils import translation
from django.views import View
from django.views.generic.edit import FormView
from django.conf import settings

from django.contrib.auth.hashers import check_password, make_password

from django.http import HttpResponse, HttpResponseRedirect

from .models import Room, Note, Reply
from .forms import *

import json
import datetime as dt
from os import urandom
import os
from stickies.settings import BASE_DIR
from PIL import Image

# Create your views here.

def room(request, roomName):
    try:
        room = Room.objects.get(roomName=roomName)
    except Room.DoesNotExist:
        room = Room(roomName=roomName,
                pubDate=timezone.now(),
                lastModified=timezone.now(),
                passwordProtection = False,
                password = "",
                customGuestRights = False,
                modPassword = "",
                )
        room.save()
    
    if "password" in request.POST.keys():
        if not request.session.session_key:
            request.session.save()
        form = roomPasswordForm(request.POST)
        if form.is_valid():
            if check_password(form.cleaned_data["password"], room.password):
                if room.authorizedusers_set.filter(sessionKey = request.session.session_key).count() == 0:
                    room.authorizedusers_set.create(sessionKey = request.session.session_key,
                                                    expireDate = timezone.now() + dt.timedelta(days=14),
                                                    lastActive = timezone.now())
                elif room.authorizedusers_set.filter(sessionKey = request.session.session_key).count() == 1:
                    user = room.authorizedusers_set.filter(sessionKey = request.session.session_key)[0]
                    user.lastActive = timezone.now()
                    user.save()
            else:
                return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": True})
        else:
            return HttpResponseRedirect("/")
    
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})


    # get Session Data
    try:
        ncols = request.session["ncols"]
    except KeyError:
        request.session["ncols"] = "auto"
        ncols = request.session["ncols"]
    try:
        sorting = request.session["sorting"]
    except KeyError:
        request.session["sorting"] = "mixed"
        sorting = request.session["sorting"]
    try:
        storeName = request.session["storeName"]
    except KeyError:
        request.session["storeName"] = False
        storeName = request.session["storeName"]
    try:
        username = request.session["username"]
    except KeyError:
        request.session["username"] = "anonymous"
        username = request.session["username"]



    # Handle sorting
    if sorting == "mixed":
        notes1 = room.note_set.filter(pubDate__gte=(timezone.now()- dt.timedelta(minutes=5))).order_by("-pubDate")
        notes2 = room.note_set.filter(pubDate__lt=(timezone.now()- dt.timedelta(minutes=5))).order_by("-likescommentscount")
        
        notes = [note.toDict() for note in notes1] + [note.toDict() for note in notes2]
        #notes = room.note_set.all()

    elif sorting == "agenewestfirst":
        notes = room.note_set.order_by("-pubDate")
        notes = [note.toDict() for note in notes]
    elif sorting == "ageoldestfirst":
        notes = room.note_set.order_by("pubDate")
        notes = [note.toDict() for note in notes]
    elif sorting == "likesmorefirst":
        notes = room.note_set.order_by("-likes")
        notes = [note.toDict() for note in notes]
    elif sorting == "likeslessfirst":
        notes = room.note_set.order_by("likes")
        notes = [note.toDict() for note in notes]
    elif sorting == "commentsmorefirst":
        notes = room.note_set.order_by("-commentscount")
        notes = [note.toDict() for note in notes]
    elif sorting == "commentlessfirst":
        notes = room.note_set.order_by("commentscount")
        notes = [note.toDict() for note in notes]
    else:        
        notes = room.note_set.all()
        notes = [note.toDict() for note in notes]
    
    for i in range(0, len(notes)):
        note = get_object_or_404(Note, pk=notes[i]["id"])
        replies = note.reply_set.order_by("pubDate")
        replies = [reply.toDict() for reply in replies]
        notes[i]["replies"] = replies
    for i in range(0,len(notes)):
        note = get_object_or_404(Note, pk=notes[i]["id"])
        imgs = note.imagefile_set.all()
        imgs = [{"id": img.id, "file": str(img.file)} for img in imgs]
        notes[i]["imgs"] = imgs


    
    context = { "roomName": roomName,
                "roomType": "wall",
                "passwordProtection" : room.passwordProtection,
                "customGuestRights": room.customGuestRights,
                #"notes": mark_safe(json.dumps(notes).replace("\'","\\\'")),
                "notes": mark_safe(json.dumps(notes)),
                "ncols": ncols,
                "sorting": sorting,
                "lastModified": room.lastModified.timestamp(),
              }
    if room.customGuestRights:
        context["guestRightsJS"] = mark_safe(json.dumps(room.customguestrights_set.all()[0].toDict()).replace("True", "true").replace("False","false"))
        context["guestRights"] = room.customguestrights_set.all()[0]
        if (room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1
            and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].expireDate > timezone.now()
            and timezone.now() - room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].lastActive < dt.timedelta(minutes=10)):
            context["isMod"] = True
    return render(request, 'wall/wall.html', context)
    #return HttpResponse("Welcome to room " + roomName)

def sessionChange(request,roomName,key,val):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
    
    request.session[key] = val
    if key == "django_language":
        request.session['language_id'] = val
        #gettext.textdomain(val)
        #translation.activate(val)
        #translation.override(val)
        #response = HttpResponseRedirect("/wall/" + roomName)
        #response.set_cookie(settings.LANGUAGE_COOKIE_NAME, val)
        #import logging
        #logging.critical(str(key) for key in request.session.keys())
        #logging.critical(request.session["django_language"])
    return HttpResponseRedirect("/wall/" + roomName)

def checkUpdate(request, roomName):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return HttpResponse(json.dumps({"msg": "passwordRequired"}))

    room = get_object_or_404(Room, roomName=roomName)

    if (room.passwordProtection and request.session.session_key and room.authorizedusers_set.filter(sessionKey=request.session.session_key).count() == 1):
        user = room.authorizedusers_set.filter(sessionKey = request.session.session_key)[0]
        user.lastActive = timezone.now()
        user.save()
    if (room.customGuestRights and request.session.session_key and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1):
        user = room.authorizedmoderators_set.filter(sessionKey = request.session.session_key)[0]
        user.lastActive = timezone.now()
        user.save()
        
    context = { "roomName": roomName,
                "passwordProtection": room.passwordProtection,
                "lastModified": room.lastModified.timestamp(),
              }
    return HttpResponse(json.dumps(context))

def update(request, roomName):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})

    room = get_object_or_404(Room, roomName=roomName)
        
    try:
        sorting = request.session["sorting"]
    except KeyError:
        request.session["sorting"] = "mixed"
        sorting = request.session["sorting"]
    # Handle sorting
    if sorting == "mixed":
        notes1 = room.note_set.filter(pubDate__gte=(timezone.now()- dt.timedelta(minutes=5))).order_by("-pubDate")
        notes2 = room.note_set.filter(pubDate__lt=(timezone.now()- dt.timedelta(minutes=5))).order_by("-likescommentscount")
        
        notes = [note.toDict() for note in notes1] + [note.toDict() for note in notes2]
        #notes = room.note_set.all()

    elif sorting == "agenewestfirst":
        notes = room.note_set.order_by("-pubDate")
        notes = [note.toDict() for note in notes]
    elif sorting == "ageoldestfirst":
        notes = room.note_set.order_by("pubDate")
        notes = [note.toDict() for note in notes]
    elif sorting == "likesmorefirst":
        notes = room.note_set.order_by("-likes")
        notes = [note.toDict() for note in notes]
    elif sorting == "likeslessfirst":
        notes = room.note_set.order_by("likes")
        notes = [note.toDict() for note in notes]
    elif sorting == "commentsmorefirst":
        notes = room.note_set.order_by("-commentscount")
        notes = [note.toDict() for note in notes]
    elif sorting == "commentlessfirst":
        notes = room.note_set.order_by("commentscount")
        notes = [note.toDict() for note in notes]

    else:        
        notes = room.note_set.all()
        notes = [note.toDict() for note in notes]
    
    for i in range(0, len(notes)):
        note = get_object_or_404(Note, pk=notes[i]["id"])
        replies = note.reply_set.order_by("pubDate")
        replies = [reply.toDict() for reply in replies]
        notes[i]["replies"] = replies
    for i in range(0,len(notes)):
        note = get_object_or_404(Note, pk=notes[i]["id"])
        imgs = note.imagefile_set.all()
        imgs = [{"id": img.id, "file": str(img.file)} for img in imgs]
        notes[i]["imgs"] = imgs

    context = { "roomName": roomName,
                "notes": notes,
                "passwordProtection": room.passwordProtection,
                "lastModified": room.lastModified.timestamp(),
              }
    
    context["customGuestRights"] = room.customGuestRights
    if room.customGuestRights:
        context["guestRightsJS"] = mark_safe(json.dumps(room.customguestrights_set.all()[0].toDict()).replace("True", "true").replace("False","false"))
        #context["guestRights"] = room.customguestrights_set.all()[0]
        if (room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1
            and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].expireDate > timezone.now()
            and timezone.now() - room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].lastActive < dt.timedelta(minutes=10)):
            context["isMod"] = True
    return HttpResponse(json.dumps(context))
    #return HttpResponseRedirect("/wall/" + roomName)

def addLike(request, roomName, noteId):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
    # Check if room is password protected, and if user is allowed to access room
    if checkPermission(request, get_object_or_404(Room,roomName=roomName),"likeNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

    room = get_object_or_404(Room, roomName=roomName)
    note = get_object_or_404(Note, pk=noteId)

    note.likes += 1
    note.likescommentscount += 1
    room.lastModified = timezone.now()

    note.save()
    room.save()

    return HttpResponseRedirect("/wall/" + roomName)
    
class addNote(FormView):
    def get(self, request, roomName):
        roomName = self.kwargs["roomName"]
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        # Check if room is password protected, and if user is allowed to access room
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        
        if "django_language" in request.session.keys() and translation.get_language() != request.session["django_language"]:
            translation.activate(request.session["django_language"])
        try:
            storeName = request.session["storeName"]
        except KeyError:
            request.session["storeName"] = False
            storeName = request.session["storeName"]
        try:
            username = request.session["username"]
        except KeyError:
            request.session["username"] = "anonymous"
            username = request.session["username"]
        form = addNoteForm()
        context = { "strTitle": gettext("Add note to %(roomName)s") % {"roomName": roomName},
                    "roomType": "wall",
                    "link": "addnote",
                    "formLink": "/wall/" + roomName + "/addnote/",
                    "roomName": roomName,
                    "subHeading": gettext_lazy("Note:"),
                    "textareaName": "noteText",
                    "textareaPlaceholder": gettext_lazy("Enter Text"),
                    "buttonSubmitText": gettext("Add note!"),
                    "form": form,
                  }
        room = get_object_or_404(Room,roomName=roomName)
        if room.customGuestRights:
            context["guestRights"] = True
            context["guestRightsAddImg"] = room.customguestrights_set.all()[0].addImg
            if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1:
                context["isMod"] = True
        return render(request, "wall/addEditContentForm.html",context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        # Check if room is password protected, and if user is allowed to access room
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
       
        form = addNoteForm(request.POST, request.FILES)

        if form.is_valid():
            # check and apply options for username storing
            checkForStoreUsername(request, form.cleaned_data)

            room = Room.objects.get(roomName=roomName)
            note = room.note_set.create(
                    noteText=form.cleaned_data["noteText"],
                    name=form.cleaned_data["name"],
                    pubDate=timezone.now(),
                    likes=0,
                    commentscount=0,
                    likescommentscount=0
                    )
                
            if len(request.FILES.getlist("images")) > 0:
                if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addImg"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

            for i in range(0, len(request.FILES.getlist("images"))):
                """ext = str(request.FILES.getlist("images")[i])[str(request.FILES.getlist("images")[i]).rfind("."):]
                if ext.lower() not in [".jpeg", ".png", ".jpg", ".svg"]:
                    context = { "strTitle": gettext("Image format not supported"),
                                "message": gettext("Currently only jpeg/jpg, png and avg are supported. You can copy your note text from above. Otherwise the note data will be lost!"),
                                "buttonCancel": False,
                                "buttonText": gettext("Back to room"),
                                "buttonLink": "/wall/" + roomName + "/",
                                "noteText": note.noteText
                              }
                    note.delete()
                    return render(request, "wall/error.html", context)"""

                img = note.imagefile_set.create(file=request.FILES.getlist("images")[i])
                imgpath = str(img.file)
                img = Image.open(os.path.join(BASE_DIR, "media/" + imgpath))
                if img.height > img.width:
                    img = img.resize((300, int(img.height/img.width*300)))
                    img = img.crop((0,max(int((img.height-300)/2),0),300,img.height-max(int((img.height-300)/2),0)))
                else:
                    img = img.resize((int(img.width/img.height*300),300))
                    img = img.crop((max(int((img.width-300)/2),0),0,img.width-max(int((img.width-300)/2),0),300))
                img.save(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))
            room.lastModified = timezone.now()
            room.save()
            return HttpResponseRedirect("/wall/" + roomName)
        else:
            HttpResponseRedirect("/wall/" + roomName + "/addnote/")

class reply(FormView):
    def get(self, request, roomName, noteId):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        # Check if room is password protected, and if user is allowed to access room
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addReply"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        try:
            storeName = request.session["storeName"]
        except KeyError:
            request.session["storeName"] = False
            storeName = request.session["storeName"]
        try:
            username = request.session["username"]
        except KeyError:
            request.session["username"] = "anonymous"
            username = request.session["username"]
        note = get_object_or_404(Note,pk=noteId)
        context = { "strTitle": gettext_lazy("Add reply"),
                    "roomName": roomName,
                    "roomType": "wall",
                    "link": str(noteId) + "/reply",
                    "reply": True,
                    "origNoteText": note.noteText,
                    "formLink": "/wall/" + roomName+ "/" + str(note.id) + "/reply/",
                    "subHeading": gettext_lazy("Reply:"),
                    "textareaName": "replyText",
                    "textareaPlaceholder": gettext_lazy("Enter Text"),
                    "buttonSubmitText": gettext_lazy("Add reply!")
                  }
        return render(request, "wall/addEditContentForm.html",context)
    def post(self, request, roomName, noteId):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addReply"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        form = addReplyForm(request.POST)

        if form.is_valid():
            note = get_object_or_404(Note,pk=noteId)
            r = note.reply_set.create(
                    replyText=form.cleaned_data["replyText"],
                    name=form.cleaned_data["name"],
                    pubDate=timezone.now()
                    )

            note.commentscount += 1
            note.likescommentscount += 1

            note.save()
            
            # check and apply options for username storing
            checkForStoreUsername(request, form.cleaned_data)

            room = get_object_or_404(Room,roomName=roomName)
            room.lastModified = timezone.now();
            room.save();
            return HttpResponseRedirect("/wall/" + roomName)
        else:
            return HttpResponseRedirect("/wall/" + roomName+ "/" + str(note.id) + "/reply/")

class editNote(FormView):
    def get(self, request, roomName, noteId):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"editNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})


        try:
            storeName = request.session["storeName"]
        except KeyError:
            request.session["storeName"] = False
            storeName = request.session["storeName"]
        try:
            username = request.session["username"]
        except KeyError:
            request.session["username"] = "anonymous"
            username = request.session["username"]

        note = get_object_or_404(Note, pk=noteId)
        context = { "strTitle": gettext_lazy("Edit note"),
                    "roomType": "wall",
                    "link": str(noteId) + "/editNote",
                    "roomName": roomName,
                    "formLink": "/wall/" + roomName+ "/" + str(note.id) + "/editNote/",
                    "subHeading": gettext_lazy("Note:"),
                    "textareaName": "noteText",
                    "textareaText": note.noteText,
                    "buttonSubmitText": gettext_lazy("Edit note!")
                  }
        if note.imagefile_set.all().count() > 0:
            context["imgsPresent"] = True

        room = get_object_or_404(Room, roomName=roomName)
        if room.customGuestRights:
            context["guestRights"] = True
            context["guestRightAddImg"] = room.customguestrights_set.all()[0].addImg
            if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1:
                context["isMod"] = True
        return render(request, "wall/addEditContentForm.html",context)
    
    def post(self, request, roomName, noteId):
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"editNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})

        form = editNoteForm(request.POST)
        if form.is_valid():
            note = get_object_or_404(Note, pk=noteId)
            room = get_object_or_404(Room, roomName=roomName)

            note.noteText = form.cleaned_data["noteText"]
            note.name = form.cleaned_data["name"]
            #note.pubDate = timezone.now()
            imgs = note.imagefile_set.all()

            if "removeImages" in request.POST.keys() and form.cleaned_data["removeImages"]:
                for img in imgs:
                    imgpath = str(img.file)
                    try:
                        os.remove(os.path.join(BASE_DIR, "media/" +  str(img.file)))
                        os.remove(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))
                    except FileNotFoundError:
                        pass
                    img.delete()

            if form.cleaned_data["images"] != "":
                if checkPermission(request, get_object_or_404(Room,roomName=roomName),"addImg"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
                for i in range(0, len(request.FILES.getlist("images"))):
                    img = note.imagefile_set.create(file=request.FILES.getlist("images")[i])
                    imgpath = str(img.file)
                    #crop image to square image
                    img = Image.open(os.path.join(BASE_DIR, "media/" + imgpath))
                    if img.height > img.width:
                        img = img.resize((300, int(img.height/img.width*300)))
                        img = img.crop((0,max(int((img.height-300)/2),0),300,img.height-max(int((img.height-300)/2),0)))
                    else:
                        img = img.resize((int(img.width/img.height*300),300))
                        img = img.crop((max(int((img.width-300)/2),0),0,img.width-max(int((img.width-300)/2),0),300))
                    img.save(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))

            room.lastModified = timezone.now()
            
            checkForStoreUsername(request, form.cleaned_data)

            note.save()
            room.save()
            return HttpResponseRedirect("/wall/" + roomName)
        else:
            return HttpResponseRedirect("/wall/" + roomName+ "/" + str(note.id) + "/editNote/")

class editReply(FormView):
    def get(self, request, roomName, noteId, replyId):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"editReply"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        try:
            storeName = request.session["storeName"]
        except KeyError:
            request.session["storeName"] = False
            storeName = request.session["storeName"]
        try:
            username = request.session["username"]
        except KeyError:
            request.session["username"] = "anonymous"
            username = request.session["username"]

        note = get_object_or_404(Note, pk=noteId)
        reply = get_object_or_404(Reply, pk=replyId)
        context = { "strTitle": gettext_lazy("Edit reply"),
                    "roomName": roomName,
                    "roomType": "wall",
                    "link": str(noteId) + "/" + str(replyId)+ "/editReply",
                    "reply": True,
                    "origNoteText": note.noteText,
                    "formLink": "/wall/" + roomName+ "/" + str(note.id) + "/" + str(reply.id) + "/editReply/",
                    "subHeading": gettext_lazy("Reply:"),
                    "textareaName": "replyText",
                    "textareaText": reply.replyText,
                    "buttonSubmitText": gettext_lazy("Edit reply!")
                  }
        return render(request, "wall/addEditContentForm.html",context)
    def post(self, request, roomName, noteId, replyId):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"editReply"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        form = addReplyForm(request.POST)

        if form.is_valid():
            note = get_object_or_404(Note, pk=noteId)
            reply = get_object_or_404(Reply, pk=replyId)
            room = get_object_or_404(Room, roomName=roomName)

            reply.replyText = form.cleaned_data["replyText"]
            reply.name = form.cleaned_data["name"]
            #note.pubDate = timezone.now()

            room.lastModified = timezone.now()
            
            checkForStoreUsername(request, form.cleaned_data)

            reply.save()
            room.save()
            return HttpResponseRedirect("/wall/" + roomName)
        else:
            return HttpResponseRedirect("/wall/" + roomName+ "/" + str(note.id) + "/" + str(reply.id) + "/editReply/")

def removeNote(request, roomName, noteId):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
    if checkPermission(request, get_object_or_404(Room,roomName=roomName),"removeNote"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

    note = get_object_or_404(Note, pk=noteId)
    room = get_object_or_404(Room, roomName=roomName)
    imgs = note.imagefile_set.all()
    for img in imgs:
        try:
            imgpath = str(img.file)
            os.remove(os.path.join(BASE_DIR, "media/" +  str(img.file)))
            os.remove(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))
        except FileNotFoundError:
            pass
    note.delete();
    room.lastModified = timezone.now()
    room.save()
    return HttpResponseRedirect("/wall/" + roomName)

def removeReply(request, roomName, noteId, replyId):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
    if checkPermission(request, get_object_or_404(Room,roomName=roomName),"removeReply"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

    reply = get_object_or_404(Reply, pk=replyId)
    note = get_object_or_404(Note, pk=noteId)
    room = get_object_or_404(Room, roomName=roomName)


    reply.delete();

    note.commentscount -= 1
    note.likescommentscount -= 1

    room.lastModified = timezone.now()
    room.save()
    return HttpResponseRedirect("/wall/" + roomName)

class delete(FormView):
    def get(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"deleteRoom"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        if "django_language" in request.session.keys() and translation.get_language() != request.session["django_language"]:
            translation.activate(request.session["django_language"])
        if not get_object_or_404(Room,roomName=roomName).passwordProtection:
            room = get_object_or_404(Room, roomName=roomName)            
            notes = room.note_set.all()
            for note in notes:
                imgs = note.imagefile_set.all()
                for img in imgs:
                    imgpath = str(img.file)
                    try:
                        os.remove(os.path.join(BASE_DIR, "media/" +  str(img.file)))
                        os.remove(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))
                    except FileNotFoundError:
                        pass
                    img.delete()

            room.delete();
            return HttpResponseRedirect("/")
        else:
            context = {"strTitle": gettext("Delete room %(roomName)s") % {"roomName": roomName},
                       "msg": gettext("You are about to delete the room %(roomName)s. If you do this, all data is lost. be careful! Please confirm by entering the password") % {"roomName": roomName},
                       "roomType": "wall",
                       "roomName": roomName,
                       "link": "delete",
                       "formLink": "/wall/" + roomName + "/delete/",
                       "passwordProtection": True,
                       "buttonCancel": True,
                       "buttonCancelLink": "/wall/" + roomName + "/",
                       "buttonCancelText": gettext_lazy("Cancel"),
                       "buttonSubmitText": gettext_lazy("Delete room")
                      }
            return render(request, 'wall/removeForm.html',context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"deleteRoom"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        form = deleteRoomForm(request.POST)

        if form.is_valid():
            room = get_object_or_404(Room, roomName=roomName)
            
            if check_password(form.cleaned_data["password"], room.password):
                notes = room.note_set.all()
                for note in notes:
                    imgs = note.imagefile_set.all()
                    for img in imgs:
                        try:
                            os.remove(os.path.join(BASE_DIR, "media/" +  str(img.file)))
                            os.remove(os.path.join(BASE_DIR, "media/" + imgpath[0:imgpath.rfind(".")] + "-cropped" + imgpath[imgpath.rfind("."):]))
                        except FileNotFoundError:
                            pass
                        img.delete()

                room.delete();
                return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/wall/" + roomName + "/delete/")
        else:
            return HttpResponseRedirect("/wall/" + roomName + "/delete/")

class addChangePassword(FormView):
    def get(self, request, roomName):
        room = get_object_or_404(Room,roomName=roomName)
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        
        if (checkPermission(request, get_object_or_404(Room,roomName=roomName),"addPassword") and not room.passwordProtection
            or checkPermission(request, get_object_or_404(Room,roomName=roomName),"changePassword") and room.passwordProtection): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        if "django_language" in request.session.keys() and translation.get_language() != request.session["django_language"]:
            translation.activate(request.session["django_language"])

        context = {"passwordProtection": room.passwordProtection,
                   "roomType": "wall",
                   "roomName": roomName,
                   "link": "addChangePassword"}
        if room.passwordProtection:
            context["strTitle"] = gettext('Change password of room %(roomName)s') % {"roomName": roomName}
            context["msg"] = gettext("You are about to change the password of %(roomName)s") % {"roomName": roomName}

        else:
            context["strTitle"] = gettext('Add password to room %(roomName)s') % {"roomName": roomName}
            context["msg"] = gettext("You are about to add a password to %(roomName)s") % {"roomName": roomName}

        return render(request, "wall/addChangePasswordForm.html",context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        room = get_object_or_404(Room, roomName=roomName)
        if (checkPermission(request, get_object_or_404(Room,roomName=roomName),"addPassword") and not room.passwordProtection
            or checkPermission(request, get_object_or_404(Room,roomName=roomName),"changePassword") and room.passwordProtection): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        form = addChangePasswordForm(request.POST)
        if form.is_valid():
            if room.passwordProtection and not check_password(form.cleaned_data["oldpassword"], room.password):
                return render(request, "wall/addChangePasswordForm.html",{"roomName": roomName, "passwordProtection": room.passwordProtection, "errormsg": gettext_lazy("Old password was wrong!")})
            elif form.cleaned_data["newpassword"] != form.cleaned_data["newpasswordrep"]:
                return render(request, "wall/addChangePasswordForm.html",{"roomName": roomName, "passwordProtection": room.passwordProtection, "errormsg": gettext_lazy("New password and repetition don't match!")})
            else:
                room.passwordProtection = True
                room.password = make_password(form.cleaned_data["newpassword"],salt=urandom(25).hex())
                for user in room.authorizedusers_set.all():
                    user.delete()
                room.authorizedusers_set.create(sessionKey = request.session.session_key,
                                               expireDate = timezone.now() + dt.timedelta(days=14),
                                               lastActive = timezone.now())
                room.lastModified = timezone.now()
                room.save()

            return HttpResponseRedirect("/wall/" + roomName)
        else:
            return HttpResponseRedirect("/wall/" + roomName + "/addChangePassword/")

class removePassword(FormView):
    def get(self, request, roomName):
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"removePassword"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})
        if "django_language" in request.session.keys() and translation.get_language() != request.session["django_language"]:
            translation.activate(request.session["django_language"])
        
        context = {"strTitle": gettext("Remove password of %(roomName)s") % {"roomName": roomName},
                   "msg": gettext("You are about to remove the password of %(roomName)s. Please confirm by entering the password") % {"roomName": roomName},
                   "roomType": "wall",
                   "roomName": roomName,
                   "link": "removePassword",
                   "formLink": "/wall/" + roomName.replace(" ","%20") + "/removePassword/",
                   "passwordProtection": True,
                   "buttonCancel": True,
                   "buttonCancelLink": "/wall/" + roomName + "/",
                   "buttonCancelText": gettext_lazy("Cancel"),
                   "buttonSubmitText": gettext_lazy("Remove password")
                  }

        return render(request, "wall/removeForm.html", context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"removePassword"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        form = removePasswordForm(request.POST)

        if form.is_valid():

            room = get_object_or_404(Room, roomName=roomName)

            if not check_password(form.cleaned_data["password"], room.password):
                if "django_language" in request.session.keys() and translation.get_language() != request.session["django_language"]:
                    translation.activate(request.session["django_language"])
                context = {"strTitle": gettext("Remove password of %(roomName)s") % {"roomName": roomName},
                           "msg": gettext("You are about to remove the password of %(roomName)s. Please confirm by entering the password") % {"roomName": roomName},
                           "formLink": "/wall/" + roomName + "/removePassword/",
                           "passwordProtection": True,
                           "buttonCancel": True,
                           "buttonCancelLink": "/wall/" + roomName + "/",
                           "buttonCancelText": gettext_lazy("Cancel"),
                           "buttonSubmitText": gettext_lazy("Remove password"),
                           "errormsg": gettext_lazy("Password is incorrect")
                          }
                return render(request, "wall/removeForm.html", context)

            room.passwordProtection = False;
            room.lastModified = timezone.now()
            for user in room.authorizedusers_set.all():
                user.delete()
            room.save()

            return HttpResponseRedirect("/wall/" + roomName)
        else:
            return HttpResponseRedirect("/wall/" + roomName + "/removePassword/")

class editGuestRights(FormView):
    def get(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"changeGuestRights"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        room = get_object_or_404(Room, roomName=roomName)
        context = {"roomName": roomName,
                   "roomType": "wall",
                   "link": "editGuestRights",
                   "customGuestRights": room.customGuestRights}
        if room.customGuestRights:
            #context["guestRightsJS"] = mark_safe(json.dumps(room.customguestrights_set.all()[0].toDict()).replace("True", "true").replace("False","false"))
            context["guestRights"] = room.customguestrights_set.all()[0]
        return render(request, "wall/editGuestRights.html", context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        if checkPermission(request, get_object_or_404(Room,roomName=roomName),"changeGuestRights"): return render(request, 'wall/moderatorLogin.html', {"roomName": roomName, "wrongPassword": False, "msg": gettext_lazy("You don't seem to have the permission to do what you are doing as a Guest. Please login as a moderator, or return")})

        form = editGuestRightsForm(request.POST)

        if form.is_valid():
            room = get_object_or_404(Room,roomName=roomName)

            if "changePassword" in request.POST.keys() and form.cleaned_data["changePassword"]:
                if form.cleaned_data["newModPassword"] != form.cleaned_data["newModPasswordRep"]:
                    context = {"roomName": roomName,
                        "customGuestRights": room.customGuestRights,
                        "errmsg": gettext_lazy("Error, new passwords do not match")}
                    if room.customGuestRights:
                        context["guestRights"] = room.customguestrights_set.all()[0]
                    return render(request, "wall/editGuestRights.html", context)
                elif not check_password(form.cleaned_data["oldModPassword"], room.modPassword):
                    context = {"roomName": roomName,
                        "customGuestRights": room.customGuestRights,
                        "errmsg": gettext_lazy("Error, old password doesnt seem to be correct")}
                    if room.customGuestRights:
                        context["guestRights"] = room.customguestrights_set.all()[0]
                    return render(request, "wall/editGuestRights.html", context)
                else:
                    room.modPassword = make_password(form.cleaned_data["newModPassword"], salt=urandom(25).hex())
                    for mod in room.authorizedmoderators_set.all():
                        mod.delete()
                    room.save()
                    room.authorizedmoderators_set.create(sessionKey = request.session.session_key,
                                                         expireDate = timezone.now() + dt.timedelta(days=14),
                                                         lastActive = timezone.now())


            room.customGuestRights = form.cleaned_data["customGuestRights"]

            if room.customGuestRights and room.customguestrights_set.all().count() == 1:
                guestRights = room.customguestrights_set.all()[0]
                guestRights.addNote = form.cleaned_data["guestRightAddNote"]
                guestRights.likeNote = form.cleaned_data["guestRightLikeNote"]
                guestRights.addImg = form.cleaned_data["guestRightAddImg"]
                guestRights.addReply = form.cleaned_data["guestRightAddReply"]
                guestRights.editNote = form.cleaned_data["guestRightEditNote"]
                guestRights.editReply = form.cleaned_data["guestRightEditReply"]
                guestRights.removeNote = form.cleaned_data["guestRightRemoveNote"]
                guestRights.removeReply = form.cleaned_data["guestRightRemoveReply"]
                guestRights.changeGuestRights = form.cleaned_data["guestRightChangeGuestRights"]
                guestRights.addPassword = form.cleaned_data["guestRightAddPassword"]
                guestRights.changePassword = form.cleaned_data["guestRightChangePassword"]
                guestRights.removePassword = form.cleaned_data["guestRightRemovePassword"]
                guestRights.changeModeratorPassword = form.cleaned_data["guestRightChangeModeratorPassword"]
                guestRights.deleteRoom = form.cleaned_data["guestRightDeleteRoom"]
                guestRights.save()
            elif room.customGuestRights and room.customguestrights_set.all().count() == 0:
                room.customguestrights_set.create(addNote = form.cleaned_data["guestRightAddNote"],
                                                  likeNote = form.cleaned_data["guestRightLikeNote"],
                                                  addImg = form.cleaned_data["guestRightAddImg"],
                                                  addReply = form.cleaned_data["guestRightAddReply"],
                                                  editNote = form.cleaned_data["guestRightEditNote"],
                                                  editReply = form.cleaned_data["guestRightEditReply"],
                                                  removeNote = form.cleaned_data["guestRightRemoveNote"],
                                                  removeReply = form.cleaned_data["guestRightRemoveReply"],
                                                  changeGuestRights = form.cleaned_data["guestRightChangeGuestRights"],
                                                  addPassword = form.cleaned_data["guestRightAddPassword"],
                                                  changePassword = form.cleaned_data["guestRightChangePassword"],
                                                  removePassword = form.cleaned_data["guestRightRemovePassword"],
                                                  changeModeratorPassword = form.cleaned_data["guestRightChangeModeratorPassword"],
                                                  deleteRoom = form.cleaned_data["guestRightDeleteRoom"],
                                                  )
                room.modPassword = make_password(form.cleaned_data["modPassword"], salt=urandom(25).hex())
                room.authorizedmoderators_set.create(sessionKey = request.session.session_key,
                                                     expireDate = timezone.now() + dt.timedelta(days=14),
                                                     lastActive = timezone.now())
                room.save()
            elif not room.customGuestRights:
                room.lastModified = timezone.now()
                for user in room.authorizedmoderators_set.all():
                    user.delete()
                for rights in room.customguestrights_set.all():
                    rights.delete()
                room.save()
                return HttpResponseRedirect("/wall/" + roomName + "/")
            else:
                return 0    


            room.lastModified = timezone.now()
            room.save()
            
            return HttpResponseRedirect("/wall/" + roomName + "/")
        else:
            return HttpResponseRedirect("/wall/" + roomName + "/editGuestRights/")

class moderatorLogin(FormView):
    def get(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        context = {"roomName": roomName,
                   "roomType": "wall",
                   "link": "moderatorLogin",
                   }
        return render(request,"wall/moderatorLogin.html",context)
    def post(self, request, roomName):
        # Check if room is password protected, and if user is allowed to access room
        if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})
        form = moderatorLoginForm(request.POST)
        if form.is_valid():
            room = get_object_or_404(Room, roomName=roomName)
            if check_password(form.cleaned_data["password"], room.modPassword):
                if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 0:
                    room.authorizedmoderators_set.create(sessionKey = request.session.session_key,
                                                         expireDate = timezone.now() + dt.timedelta(days=14),
                                                         lastActive = timezone.now())
                    room.save()
                elif room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1:
                    user = room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0]
                    user.lastActive = timezone.now()
                    user.save()
                else: return 0

                return HttpResponseRedirect("/wall/" + roomName + "/")
            else:
                return render(request,"wall/moderatorLogin.html", {"wrongPassword": True, "roomName": roomName})
        else:
            return HttpResponseRedirect("/wall/" + roomName + "/moderatorLogin/")

def moderatorLogout(request, roomName):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})

    room = get_object_or_404(Room,roomName=roomName)

    if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() != 0:
        for user in room.authorizedmoderators_set.filter(sessionKey=request.session.session_key):
            user.delete()
        return HttpResponseRedirect("/wall/" + roomName + "/")
    else:
        return HttpResponseRedirect("/wall/" + roomName + "/")

def roomLogout(request, roomName):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})

    room = get_object_or_404(Room,roomName=roomName)
    if room.passwordProtection and room.authorizedusers_set.filter(sessionKey=request.session.session_key).count() != 0:
        for user in room.authorizedusers_set.filter(sessionKey=request.session.session_key):
            user.delete()
    if room.customGuestRights and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() != 0:
        for user in room.authorizedmoderators_set.filter(sessionKey=request.session.session_key):
            user.delete()


    return HttpResponseRedirect("/")



def error(request,roomName, errorType):
    context = {}
    if errorType == "roomRemoved":
         context = { "strTitle": gettext_lazy("Room deleted"),
                     "message": gettext_lazy("This room isn't accessible anymore, maybe it was deleted? Feel free to recreate the room, but all data is lost. Or go back to the index and create a new room!"),
                     "buttonCancel": True,
                     "buttonCancelText": gettext_lazy("Back to home"),
                     "buttonCancelLink": '/',
                     "buttonText": gettext_lazy("Recreate room"),
                     "buttonLink": "/wall/" + roomName + "/"
                    }


    return render(request, "wall/error.html", context)

def images(request, roomName, noteId):
    # Check if room is password protected, and if user is allowed to access room
    if checkPasswordProtection(request, get_object_or_404(Room,roomName=roomName)): return render(request, 'wall/passwordProtectedRoom.html', {"roomName": roomName, "wrongPassword": False})

    note = get_object_or_404(Note, pk=noteId)
    imgs = [{"img": img.id, "file": img.file} for img in note.imagefile_set.all()]
    context = {"noteText": note.noteText,
               "roomName": roomName,
              "imgs" : imgs}
    return render(request, "wall/images.html", context)


# Some functions
def checkForStoreUsername(request, data):
    if data["storeName"]:
        request.session["username"] = data["name"]
        request.session["storeName"] = True
    else:
        request.session["storeName"] = False
        request.session["username"] = "anonymous"


def checkPasswordProtection(request, room):
    roomName = room.roomName
    if ((room.passwordProtection and room.authorizedusers_set.filter(sessionKey=request.session.session_key).count() == 0)
       or (room.passwordProtection and room.authorizedusers_set.filter(sessionKey=request.session.session_key)[0].expireDate < timezone.now() )
       or (room.passwordProtection and timezone.now() - room.authorizedusers_set.filter(sessionKey=request.session.session_key)[0].lastActive > dt.timedelta(minutes=10))):
        context = {"roomName": roomName,
                   "wrongPassword": False}
        return True
    else:
        return False

def checkPermission(request, room, key):
    roomName = room.roomName
    if room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 1 and (room.customGuestRights and timezone.now() - room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].lastActive > dt.timedelta(minutes=10)):
        room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].delete()
    if (((room.customGuestRights and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key).count() == 0)
            or (room.customGuestRights and room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].expireDate < timezone.now())
            or  (room.customGuestRights and timezone.now() - room.authorizedmoderators_set.filter(sessionKey=request.session.session_key)[0].lastActive > dt.timedelta(minutes=10)))
            and room.customguestrights_set.all()[0].toDict()[key] == False):
        return True
    elif not room.customGuestRights:
        return False
    else: return False
