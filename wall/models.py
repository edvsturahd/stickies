from django.db import models
from django.conf import settings
#from django.utils import timezone

#import os


# Create your models here.

class Room(models.Model):
    roomName = models.CharField(max_length=100, unique=True)
    pubDate = models.DateTimeField('date published')
    lastModified = models.DateTimeField('date published')
    passwordProtection = models.BooleanField(default=False)
    password = models.CharField(max_length=1024, default="")
    customGuestRights = models.BooleanField(default=False)
    modPassword = models.CharField(max_length=1024, default="")

    #passwordSalt = models.CharField(max_length=100, default="")

    def __str__(self):
        return self.roomName

class Note(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    noteText = models.CharField(max_length=2000)
    name = models.CharField(max_length=50)
    likes = models.IntegerField(default=0)
    commentscount = models.IntegerField(default=0)
    likescommentscount = models.IntegerField(default=0)
    pubDate = models.DateTimeField('date published')
    #pubDate = models.DateTimeField('date published',default=0)
    def toDict(self):
        return {"room": self.room.roomName,
                "id": self.id,
                "noteText": self.noteText,
                "name": self.name,
                "likes": self.likes,
                "pubDate": self.pubDate.timestamp(),
                }
class Reply(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    replyText = models.CharField(max_length=2000)
    name = models.CharField(max_length=50)
    pubDate = models.DateTimeField("date published")

    def toDict(self):
        return {"note": self.note.id,
                "id": self.id,
                "replyText": self.replyText,
                "name": self.name,
                "pubDate": self.pubDate.timestamp(),
                }

class AuthorizedUsers(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    sessionKey = models.CharField(max_length = 40)
    expireDate = models.DateTimeField("expiry date of session")
    lastActive = models.DateTimeField("date of last activity")
class AuthorizedModerators(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    sessionKey = models.CharField(max_length = 40)
    expireDate = models.DateTimeField("expiry date of session")
    lastActive = models.DateTimeField("date of last activity")

class CustomGuestRights(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    addNote = models.BooleanField()
    likeNote = models.BooleanField()
    addImg = models.BooleanField()
    addReply = models.BooleanField()
    editNote = models.BooleanField()
    editReply = models.BooleanField()
    removeNote = models.BooleanField()
    removeReply= models.BooleanField()
    changeGuestRights = models.BooleanField()
    addPassword = models.BooleanField()
    changePassword = models.BooleanField()
    removePassword = models.BooleanField()
    changeModeratorPassword = models.BooleanField()
    deleteRoom = models.BooleanField()
    def toDict(self):
        return {
                "addNote": self.addNote,
                "likeNote": self.likeNote,
                "addImg": self.addImg,
                "addReply": self.addReply,
                "editNote": self.editNote,
                "editReply": self.editReply,
                "removeNote": self.removeNote,
                "removeReply": self.removeReply,
                "changeGuestRights": self.changeGuestRights,
                "addPassword": self.addPassword,
                "changePassword": self.changePassword,
                "removePassword": self.removePassword,
                "changeModeratorPassword": self.changeModeratorPassword,
                "deleteRoom": self.deleteRoom,
                }

class ImageFile(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    file = models.ImageField(upload_to="wall/")



