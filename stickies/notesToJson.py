from django import template
from django.utils.safestring import mark_safe

import json
register = template.Library()


@register.filter(is_safe=True)
def notesToJson(notes):
    ret = []
    for note in notes:
        ret += [{"noteText": note.noteText}]
    return mark_safe(json.dumps(obj))

