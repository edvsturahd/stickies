from django.utils.safestring import mark_safe
from django.template import Library

import json


register = Library()


@register.filter(is_safe=True)
def js(obj):
    ret = []
    for note in notes:
        ret += [{"noteText": note.noteText}]
    return mark_safe(json.dumps({"test": "Hello world"}))
