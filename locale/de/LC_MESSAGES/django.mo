��    <      �  S   �      (  I   )  �   s  i  ?  7  �	  K  �  l  -  !  �  =  �  	  �  >    �  C  �       �  �  �    n!     w(  "  �)  y   �*    5+  �  O-     D0  	   J0     T0     a0     m0     |0     �0     �0     �0     �0     �0     �0     �0     �0      1     1     1     ,1     =1     Q1     e1     t1     �1  
   �1  
   �1     �1     �1     �1     �1     �1     �1     	2     2     52     A2     J2     ]2     p2     |2  B  �2  :   �3  �   4  �  �4  �  �8  7  �<  �  �=  7  y@  J  �A  #  �B  �   D  �  �H  �   �J  �   wK  '  aL    �R  J  �Z  ~  �[  f   k]  W  �]  �  *`     	c     c     #c     8c     Nc     Wc     hc     }c     �c      �c  
   �c     �c     �c     �c  	   �c     �c     d     "d     4d     Cd     Rd     ^d     qd  	   �d  	   �d     �d     �d     �d     �d     �d     �d     �d     e     1e     ?e     Ge     ^e     re  
   �e            5   :   3   #                                      '              (   )   0       <   ;   "                    +            %       ,                 *   
       1       9           .       	             &       4   6       7   /                    !      $      2                  -   8       
			  This message should be edited by whoever deployed this website.
			 
			  Welcome to Stickies! Stickies is an easy to use collaborative bulletin board like App, where you can share ideas by putting virtual sticky notes on a board with your friends or your colleagues.
			 
			Creating a room as totally easy. Simply go to the <a href="/">home page</a>, enter a name for the room, select a <a href="#roomTypes">room a type</a> and click create! You can add <a href="#guestRights">guest rights</a> or <a href="#passwordProtection">protect your room with a password</a>. After then <strong>share the link</strong> (simply copy it from your browser) with the people you want to invite to your room.<br>
			Without <a href="#passwordProtection">password protection</a> your room will be available to anyone who has (or guesses!) the link to your room, and without <a href="#guestRights">guest rights</a> anyone who has (or again guesses) the link to the room, can meddle around in it!<br>
			But don't worry too much about that at the beginning, you can always change these options when using the room in the menu (<a href="#wallMenu">Wall</a>).

			 
			In general this website only uses necessary cookies, such that the website runs properly. No data is given to any third parties. <br><br>
			A cookie with a session_key is created and related to data if:
			<ul>
				<li>you change the language to something else which is not your default (lang. option is stored)</li>
				<li>you change the number of cols (option is stored)</li>
				<li>you change the sorting method (sorting method ist stored)</li>
				<li>you choose to store the username (suprisingly the username ist stored)</li>
				<li>you are a moderator of a room with customized guest rights (time of creation and last activity time is stored)</li>
				<li>you are in a password protected room (time of creation and last activity time is stored)</li>
			</ul>
			That's it. Shouldn't be too personal data.
			 
			Stickies! was created by a member of the <a href="https://www.stura.uni-heidelberg.de/vs-strukturen/referate/edv/">EDV-Referat</a> (IT-Department) of the <a href="www.stura.uni-heidelberg.de">Studierendenrat</a> (Student's union) at <a href="www.uni-heidelberg.de">Heidelberg University</a>, and is still under development.
			 
			The bulletin board is a very simple roomtype, where users can just add notes (with images), reply to them and likes them. They will be arranged automatically, you can adjust a sorting method and the number of columns in the menu (gear icon in the top right).<br>
			The <strong>mixed sorting</strong> puts new notes (from the last five minutes) first, thus giving new notes the possibility to be read first. Notes older than five minutes are orderd by a mixture of amount of comments and amount of likes (where both count as one).
			<br>
			The other <strong>sorting</strong> methods should be self explanatory.
			 
			This app is written using the <a href="https://www.djangoproject.com">django webframework</a> and the source code can be found at <a href="https://gitlab.com/edvsturahd/stickies/-/tree/main#translation">gitlab</a> with further information and help on how to deploy your own server.
			 
			Welcome to Stickies! Stickies is an easy to use collaborative bulletin board like App, where you can share ideas by putting virtual sticky notes on a board with your friends or your colleagues. It is meant to be <strong>open source</strong>, <strong>privacy friendly</strong> and <strong>easy to use</strong>.
			 
			You found and error, or spotted some problem: Great! Either report it directly to <a href="https://gitlab.com/edvsturahd/stickies/-/issues">gitlab</a> or write an <a href="mailto:stickies@stura.uni-heidelberg.de">e-mail</a>.
			<br>
			<br>
			Thanks a lot!
			 
		By default anyone can do anything in a room. This includes editing and deleting messages, replies, or even deleting the room. There are more evil possibilites, but let's not delve to deep into these issues. If you don't want users to be able to meddle around too much, you can adjust the rights for your guest. You could boil their rights down to just watching, but keep in mind, that that's probably a bit boring. Sensible choice would be to forbid users to remove or edit notes, delete the room etc.<br>
		If you introduce guest rights, you also have to add a <strong>moderator password</strong>, thus having at least one person as moderator which is able to do anything. If you create a room with customized guest rights, you'll be automatically added to the list of moderators.<br>
		If you also use <a href="#passwordProtection">password protection</a> on your room, make sure to chose different passwords, as the people you invite should know the password for the room, but should not necessarily know the moderator password, which would allow them to do damage to the room.
		 
		Editing notes and replies is just like adding <a href="#addNote">notes</a> and <a href="#addReply">replies</a>. You'll find an edit button at the bottom of the notes or replies. There is a small difference when editing notes, you can just either <strong>remove all</strong> images, or none at all. There is a workaround if you want to delete one or just some images, by deleting all of them and reuploading just those images, that you want to keep in your note.
		 
		For now Stickies! only has a <a href="#bulletinBoard">bulletin board</a> room type. Another type called <a href="#drawers">drawers</a> is planned
		 
		If there are notes and replies in the room, there will be a button at the bottom of each, which will allow you to delete them. But careful, once you've deleted something, it is gone forever and <strong>can not be undone!</strong> You'll have to confirm a popup.
		 
		If you don't want anyone (who knows or guesses) the room link to be able to access your room, you can add password protection. If you're not very creative, you can use the generate button, which will generate a password for you. Make sure to <strong>copy and store</strong> it somewhere, since you need to share the password with people, and you need it if you want to log in some later time, or even if you want to delete your room.<br>
		The random password generator uses linuxes urandom, and <strong>should</strong> be pretty secure. However, even if that was not good, storing or sharing the password should be the easier attack vectors. Yes the password generator gives <em>just</em> a hexadecimal number, but it is a common misconeption, that passwords should include numbers, uppercase or lowercase characters and whatever else weird symbols come to mind. Understanding <a href="https://en.wikipedia.org/wiki/Exponentiation">exponential functions</a>, one comes to the conclusion that the exponent, thus password length, is way more important than the base (amount of possibly used characters).<br>
		Only hashed passwords are stored, with a salt generated by urandom.<br>
		<strong>CAREFUL:</strong> Images will <strong>always</strong> be <strong>publicly available</strong>. <strong>No matter</strong> if the room was <strong>password protected</strong>. This is due to the fact that the media files are served by nginx and not django itself.
		 
		In the top right corner of the header you will find a gear icon. If you hover over it with your cursor, a menu will apear. <br>
		In this menu you can:
		<ul>
			<li>
				<strong>change the language</strong>, though currently only german and english are supported, but you can <a href="https://gitlab.com/edvsturahd/stickies/-/tree/main#translation" target="_blank">help translating</a>
			</li>
			<li>
				choose <strong>the number of columns</strong> that should be displayed in the browser. Too many are leads to narrow notes, and too few lead to very broad notes. The <strong>auto</strong> setting, whill try to find a good number automatically.
			</li>
			<li>
				select <strong>sorting</strong> method, where the <strong>auto</strong> method puts new notes (less than 5 minutes old) first, and then sorts older messages by a mixture of likes and amount of comments (each weighted as one). The other options should be self explanatory.
			</li>
			<li>
				Depending on your settings you can <strong>add, change and remove <a href="#passwordProtection">passwords</a></strong> in the menu. Of course you can add a password, if the room was not password protected, and only change a password if the room is password protected. Similarly for removing passwords.
			</li>
			<li>
				<strong>Add or edit <a href="#guestRights">guest rights</a></strong> if you haven't done so when creating the room.
			</li>
			<li>
				<strong>Login or logout as moderator</strong> if the room has customized guest rights. Moderators will then again be allowed to do all the stuff, that is forbidden for guests.
			</li>
			<li>
				<strong>Deleting the room</strong> yeah well deletes the room with all it's containing data. Once a room is deleted, <strong>all data is lost!</strong>
			</li>
			
		</ul>
		 
		Once someone <a href="#addNote">added a note</a>, you can also add a reply by simply clicking on the reply button at the bottom of the note. Again you can choose to have a username once, and even store to rember it. When answering to a note, you <strong>can not add images</strong>.
		 
		This is basically just a repition. by clicking on the white plus on red background you can add a note. A form should pop up and you can enter you note with Text. You can also remember the username, or just use a name for one note. Adding <a href="#images">images</a> is also possible.
		 
		Using rooms should be pretty straight forward, but feel free to continue reading, if you are puzzeled at some point
		 
		Using the bulletin board type is easy, just start with adding a first note by clicking on the big white plus on red background, a form should show up, and you can Enter your fist note :). <br>
		By default the <strong>username</strong> is anonymous, of course you can just leave it at that. But you can also enter a username for just once, or click on the <strong>store username</strong> checkbox, to remember the username for the next note, reply, or even for edits. With the image file field you can also add images to your note.
		 
		When adding notes, you can also add images (of course also a single image). Up to three will be shown in the note as preview, if you've added more than three images, a notification will show up, to click on one image for all images. If you click on any image (no matter how many there are) you'll be redirected to another page which shows all images (yes even more than three) in a higher resolution. In this image page you can click on an image again to get the full image.<br>
		<strong>CAREFUL:</strong> Images will <strong>always</strong> be <strong>publicly available</strong>. <strong>No matter</strong> if the room was <strong>password protected</strong>. This is due to the fact that the media files are served by nginx and not django itself.
		 About Add notes Add password Add replies Bulletin board Change password Creating a room Delete notes and replies Edit guest rights Edit notes and replies Guest rights Help Images Language Legal notice Logout of room Moderator login Moderator logout Password Protection Password protection Privacy notice Remove password Report an Issue Room Types Room types Table of contents Using Rooms Using rooms Using the menu age (newest first) age (oldest first) comments (less first) comments (more first) delete room language likes (less first) likes (more first) no. of cols sorting Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
Diese Seite sollte von einem Admin bearbeitet werden.
			 
Willkommen bei Stickies! Stickies ist eine einfache kollaborative Pinnwand , bei der du Ideen mit Hilfe von virtuellen Haftnotizen Ideen mit Freunden und Kollegen teilen kannst.
			 
Es ist total einfach einen Raum zu erstellen, gehe einfach auf die <a href="/">Hauptseite</a> gib einen Namen für den Raum ein, wähle einen <a href="#roomTypes">Raumtypen</a> und klicke auf erstellen! Du kannst <a href="#guestRights">Gastrechte</a> hinzufügen, oder den Rau mit einem <a href="#passwordProtection">Passwort</a> schützen. Danach musst du nur noch <strong>den Link teilen</strong> (einfach vom Browser rauskopieren), mit Menschen, die du ein den Raum einladen möchtest.<br>Wenn du den raum nicht mit einem <a href="#passwordProtection">Passwort</a> schützt, ist der Raum für alle Menschen die den Link kennen (oder erraten) zugänglich. Genau so kann jeder der den Link kennt (oder mal wieder errät) Unfug in dem Raum treiben, wenn du keine <a href="#guestRights">Gastrechte</a> einstellst. Keine Sorge, du kannst das alles auch noch im nachhinein ergänzen, wenn du in dem Raum die entsprechend Optionen im Menü wählst (<a href="#wallMenu">Pinnwand</a>)
			 
			Normalerweise nutzt diese Website nur Cookies die zwingend erforderlich sind, sodass die Webseite benutzt werden kann. Es werden selbstverständlich keine Daten an Dritte weitergegeben. <br><br>
			Ein Cookie mit einem session_key wird erstellt und mit Daten in Verbindung gebracht falls:
			<ul>
				<li>du die Sprache änderst (die Spracheoption wird gespeichert) </li>
				<li>du die Anzal der Spalten änderst (Spaltenanzahl wird gespeichert)</li>
				<li>du die Sortierungsmethode änderst (Sortierungsmethode wird gespeichert)</li>
				<li>du dich dazu entscheidest, den Nutzernamen zu speichern (Welch Überraschung, hier wird der Nutzername gespeichert)</li>
				<li>du der Moderator eines Raumes mit eingeschränkten Gastrechten bist (Zeit der Erstellung und letzten Aktivität werden gespeichert)</li>
				<li>Wenn du einem Passwortgeschützen Raum beitrittst (Zeit der Erstellung und der letzten Aktivität wird gespeichert.)</li>
			</ul>
			Das ist alles, sollten nicht all zu persönliche Daten sein.
			 
Stickies! wurde programmiert von einem Referenten des <a href="https://www.stura.uni-heidelberg.de/vs-strukturen/referate/edv/">EDV-Referats</a> des <a href="www.stura.uni-heidelberg.de">Studierendenrates</a> der <a href="www.uni-heidelberg.de">Universität Heidelberg</a>, und wir derzeit noch entwickelt.
			 
Die Pinnwand ist ein ser einfacher Raum Modus, bei dem Nutzer*innen einfach Notizen (mit Bildern) hinzufügen, Notizen antworten, und Notizen liken können. Die Notizen werden automatisch soritert, die Methode der Sortierung kann eingestellt werden, genau so wie die Anzahl der Spalten, siehe dazu das Zahnrad in der oberen rechten Ecke.<br>Die <strong>gemischte</strong> Sortierung sortiert neue Notizen (innerhalb der letzten fünf Minuten) zuerst, damit neuere eher gelesen werden, und sortiert ältere Notizen dann nach einer Mischung aus Likes und anzahl der Kommentar (gewichtung eins zu eins).<br>Die anderen <strong>Sortierungsmethoden</strong> sollten selbsterklärend sein.
			 
Diese app wurde mit dem <a href="https://www.djangoproject.com">Django Webframework</a> entwickelt und der Quellcode findet sich auf <a href="https://gitlab.com/edvsturahd/stickies/-/tree/main#translation">gitlab</a> mit weitern Informationen, und Hilfe, falls du deinen eigenen Server aufsetzen möchtest.
			 
Willkommen bei Stickies! Stickies ist eine einfache kollaborative Pinnwand , bei der du Ideen mit Hilfe von virtuellen Haftnotizen Ideen mit Freunden und Kollegen teilen kannst. Dabei soll Stickies <strong>open source</strong>, <strong>Datensparsam</strong>  und <strong>simpel</strong> sein und <strong>simpel</strong> sein.
			 
			Du hast einen Fehler gefunden, oder ein Problem entdeckt?: Super! Melde das doch direkt entweder bei <a href="https://gitlab.com/edvsturahd/stickies/-/issues">gitlab</a>, oder schreibe eine <a href="mailto:stickies@stura.uni-heidelberg.de">e-mail</a>.
			<br>
			<br>
			Vielen Dank!
			 
Standardmäßig kann jeder alles in einem Raum tun. Das beinhaltet das bearbeiten oder löschen von Notizen und Antworten, sowie auch einfach das Löschen eines Raumes. Es gibt sicherlich noch weitere böse Dinge, aber kommen wir nun zum Punlt. Wenn du nicht möchtest, das Nutzer*innen zu viel Unfug treiben können, kann du die Rechte für Gäste bearbeiten. Man könnte die Rechte soweit runterbrechen, dass sie nur noch zuschauen können, aber das könnte etwas langweilig werden. Sinvoll wäre z.B. es Gästen zu verbieten Dinge zu löschen, u.s.w. <br>Wenn du Gastrechte benutzt, musst du ein <strong>Moderatorenpasswort</strong> hinzufügen, um so mindestens eine Person zu haben, die in dem Raum alles mahcen kann. Wenn du einen Raum mit eigenen Gastrechten erstellst, wirst du automatisch zu der Moderatorenliste hinzugefügt. <br>Wenn du den Raum auch mit einem <a href="#passwordProtection">Passwort</a> schützen möchtest, stelle sicher, dass es verschiedene Passwörter sind, da du das Raumpasswort auch mit den Menschen geteilt werden muss, die du einladen möchtest, jedoch diese Menschen nicht unbedingt Moderatorenrechte haben sollen, die potentiell erlauben, Schaden anzurichten.
		 
Notizen und Antworten bearbeiten ist quasi genau so wie <a href="#addNote">Notizen</a> und <a href="#addReply">Antworten</a> hinzufügen. Du findest den Bearbeitenbutton am Ende von Antworten und Notizen. Es gibt einen kleinen Unterschied beim bearbeiten von Notizen, du kann nur entweder <strong>alle Bilder löschen</strong> oder keins. Workaround: wenn man nur einzelne Bilder löschen möchte, einfach alle Bilder löschen, und die Bilder, die bei der Notiz bleiben sollen, erneut anhängen.
		 
Bisher gibt es bei Stickies! nur den <a href="#bulletinBoard">Pinnwand Modus</a>. Ein weiter Mouds names <a href="#drawers">Schubladen</a> ist geplant und in Entwicklung.
			 
Wenn es in Räumen Notizen und Antworten gibt, findet sich unter Ihnen ein Buttom zum löschen von ihnen. Sei dabei aber Vorsichtig, einmal gelöscht ist es weg, und kann <strong>nicht wieder Rückggängig gemacht</strong> werden
		 
Wenn du nicht möchtest, dass jeder der den Link kennt (oder ihn mal wieder errät) Zugriff auf den Raum hat, kannst du den Raum mit einem Passwort schützen. Wenn du nicht so kreativ bist, kannst du den generieren Knopf benutzen, der dir ein Passwort erstellt. <strong>Kopiere und speichere</strong> das Passwort irgendwo, du musst es ja mit den Menschen, die du in den Raum einladen möchtest teilen, außerdem brauchst du es eventuell später nochmal, bspw. wenn du wieder kommst, oder den Raum löschen möchtest.<br>Der Zufallsgenerator nutzt Linux' urandom Funktion, die <strong>ziemlich</strong> sicher sein sollte. Wie auch immer, der gefährlichere Angriffsvektor ist wahrscheinlich eher das speichern und teilen des Passworts. Ja der Passwortgenerator spuckt <em>nur</em> Hexadezimalzahlen aus, aber es ist ein allgemeines Missverständnis, dass Passworter Ziffern, Groß-, Kleinbuchstaben und was für unsinnige Zeichen einem noch einfallen enthalten sollte. Mit dem Verständnis von <a href="https://de.wikipedia.org/wiki/Potenz_(Mathematik)">Exponentialfunktionen</a> wird klar, dass der Exponent (also die länge des Passworts) sehr viel wichtiger ist, als die Basis (die Anzahl der möglich verwendeten Zeichen).<br>Es werden nur gehashte Passwörter gespeichert die mit einem Salt von urandom gespeist werden.<strong>VORSICHT:</strong> Bilder werden <strong>immer öffentlich zugänglich</strong> sein. Ein <strong>Passwortschutz</strong> ist dabei <strong>völlig egal.</strong> Das liegt daran, dass nginx Mediendateien liefert, und nicht Django selbst.
		 
		Im oberen rechten Eck des Headers findest du ein Zahnrand Icon, wenn du mit deiner Maus darüber bist, öffnet sich das Menü. <br>
		In diesem Menü kannst du:
		<ul>
			<li>
				Die <strong>Sprache ändern</strong>, obwohl im Moment nur Deutsch und Englisch verfügbar sind, aber du kannst gerne beim <a href="https://gitlab.com/edvsturahd/stickies/-/tree/main#translation" target="_blank">Übersetzen helfen!</a>
			</li>
			<li>
				Wähle die <strong>Anzahl der Spalten</strong> die in deinem Browser angezeigt werden sollen. Zu viele Spalten sorgen für zu schmale Notizen, und zu wenige zu zu breiten Notizen. Die Einstellung <strong>auto</strong> versucht automatisch eine gute Anzahl zu finden.
			</li>
			<li>
				Suche dir eine <strong>Sortierung</strong>smethode aus, wobei die Einstellung <strong>auto</strong> neue Notizen vorsortiert (weniger als fünf Minuten alte), und danach ältere Notizen mit einer Mischung aus Likes und Anzahl der Kommentare (jeweils mit eins gewichtet) sortiert. Die restlichen Methoden sollten selbsterklärend sein.
			</li>
			<li>
				Je nach Raumeinstellung kannst du im Menü <strong><a href="#passwordProtection">Passwörter</a> hinzufügen, oder ändern und entfernen </strong>. Natürlich kannst du nur ein Passwort hinzufügen, wenn vorher noch keines gesetzt war, und das Passwort nur ändern, wenn der Raum Passwortgeschützt war (Analog für das Entfernen des Passworts).
			</li>
			<li>
				<strong><a href="#guestRights">Gastrechte</a> hinzufügen und entfernen</strong> falls das bei der Erstellung des Raumes noch nicht passiert ist.
			</li>
			<li>
				Als <strong>Moderator ein- und ausloggen</strong> fall der Raum eingeschränkte Gastrechte hat. Moderatoren sind dann wieder in der Lage alles zu tun, was Gästen verboten ist.
			</li>
			<li>
				<strong>Raum löschen</strong> nunja, löscht den Raum mit all seinen Daten. Wenn ein Raum gelöscht wurde, sind <strong>alle Daten verloren!</strong>. Man kann das Löschen reines Raumes <strong>nicht Rückgängig</strong> machen.
			</li>
			
		</ul>
		 
Sobald jemand eine <a href="#addNote">Notiz</a> hinzufügt, kann du antworten, in dem du auf den Antwortbutton am Ende der Notiz drückst. Du kannst die wieder aussuchen, ob du einen Nutzernamen einmalig hinzufügen oder auch speuchen möchtest. Beim beantworten einer Notiz kann man <strong>keine Bilder</strong> hinzufügen.
		 
Das ist im Prinzip nur eine Wiederholung: Wenn du auf das große weiße Plus auf rotem Hintergrund drückst, kannst du Notizen hinzufügen. Ein Formular öffnet sich, und du kannst deine Notiz mit einem Text hinzufügen. Du kannst dafür sorgen, dass sich die Website deinen Namen merkt, oder einen Namen nur einmal Nutzen. Man kann auch <a href="#images">Bilder</a> hinzufügen
		 
Räume zu nutzen sollte recht einfach sein, aber lies gerne weiter, wenn dich irgendwas irritiert.
		 
Der Pinnwand Modus ist einer der langweiligstens, füge einfach deine erste Notiz hinzu, in dem du auf das große weiße Plus auf rotem Hingergrund klickst. Daraufhin sollte sich ein Formular öffnen, in dem du deine erste Notiz hinzufügen kannst :).<br>Standardmäßig ist der <strong>Nutzername</strong> anonymous, du kannst natürlich einfach dabei bleiben, oder auf <strong>Nutzernamen speichern</strong> drückst. Du kannst aber auch einen Nutzernamen nur einmalig nutzen, indem du nicht auf speicher drückst. Mit dem Bilder hinzufügen Knopf kannst du auch Bilder zu Notizen hinzufügen!
		 
Wenn du Notizen hinzufügst, kannst du auch Bilder (bzw. ein einzelnes Bild) anhängen. Bis zu drei Bilder werden als Vorschau angezeigt, wenn du mehr als drei Bilder angehängt hast, wird eine Informationi angezeigt, dass weitere Bilder durch klicken angezeigt werden. Wenn du auf irgendeines der Bilder klickst, öffnet sich eine neue Seite, bei der du alle Bilder in besserer Qualität betrachten kannst, und wenn du dort nochmal auf ein Bild klickst, bekommst du das Original.<br><strong>VORSICHT:</strong> Bilder werden <strong>immer öffentlich zugänglich</strong> sein. Ein <strong>Passwortschutz</strong> ist dabei <strong>völlig egal.</strong> Das liegt daran, dass nginx Mediendateien liefert, und nicht Django selbst.
		 Über Notizen hinzufügen Passwort hinzufügen Antworten hinzufügen Pinnwand Passwort ändern Einen Raum erstellen Notizen und Antworten entfernen Gastrechte bearbeiten Notizen und Antworten bearbeiten Gastrechte Hilfe Bilder Sprache Impressum Aus Raum ausloggen Moderatorenlogin Moderatorenlogout Passwortschutz Passwortschutz Datenschutz Passwort entfernen Problem melden Raumtypen Raumtypen Inhalt Räume benutzen Räume benutzen Das Menü benutzen Alter (neuest zuerst) Alter (älteste zuerst) Kommentare (weniger zuerst) Kommentare (mehr zuerst) Raum löschen Sprache Likes (weniger zuerst) Likes (mehr zuerst) Spaltenanzahl Sortierung 